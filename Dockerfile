FROM php:7.2-fpm
#definimos la raviable de la ruta
ENV WORKDIR /var/www
# directorio a trabajar
WORKDIR ${WORKDIR}/
# Instalamos dependencias
RUN apt-get update && apt-get install -y \
    build-essential \
    locales \
    git \
    unzip \
    zip \
    curl \
    libpq-dev
# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
# Instalamos las extensiones
RUN docker-php-ext-install pdo pdo_pgsql mbstring  exif pcntl
# Instalamos composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#copiamos los archivos
COPY composer.* ${WORKDIR}/
#copiar el proyecto en el directorio
COPY . ${WORKDIR}/
#damos permisos
RUN find ${WORKDIR}/ -type d -exec chmod 755 "{}" \; && \
    find ${WORKDIR}/ -type f -exec chmod 644 "{}" \; && \
    chmod -R 777 ${WORKDIR}/storage ${WORKDIR}/bootstrap/cache
#exponemos el puerto 9000
EXPOSE 9000
CMD ["php-fpm"]
